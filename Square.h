#include <iostream>

#include "Shape.h"
#include "Matrix.h"

using namespace std;

#ifndef LABA_SQUARE_H
#define LABA_SQUARE_H

class Square : public Shape {
public:

    static const int SPACE = 600;
    static const int WIDTH = 640 + SPACE;
    static const int HEIGHT = 480 + SPACE;

    Matrix *mt;
    Matrix *dm;
    int bitmap[WIDTH][HEIGHT];

    const int A = 100;

    int angle = 0;

    Square() {
        mt = new Matrix();
        dm = new Matrix();
        mt->createM1(4, 3);
        dm->createM1(4, 3);

        mt->mas[0][0] = 0;
        mt->mas[0][1] = 0;

        mt->mas[1][0] = A;
        mt->mas[1][1] = 0;

        mt->mas[2][0] = A;
        mt->mas[2][1] = A;

        mt->mas[3][0] = 0;
        mt->mas[3][1] = A;

        mt->printMatrix();
    }


    void clearBitmap() {
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                bitmap[i][j] = 0;
            }
        }
    }

    void zakr(int x, int y) {
        if (bitmap[x][y] == 0) {
            bitmap[x][y] = 1;
            zakr(x + 1, y);
            zakr(x - 1, y);
            zakr(x, y + 1);
            zakr(x, y - 1);
        }
    }

    void drawLine(int x1, int y1, int x2, int y2) {
        x1 += SPACE;
        x2 += SPACE;
        y1 += SPACE;
        y2 += SPACE;
        double deltaX = abs(x2 - x1);
        double deltaY = abs(y2 - y1);
        double signX = x1 < x2 ? 1 : -1;
        double signY = y1 < y2 ? 1 : -1;

        double error = deltaX - deltaY;

        bitmap[x2][y2] = 1;
        while(x1 != x2 || y1 != y2) {
            bitmap[x1][y1] = 1;
            double error2 = error * 2;

            if(error2 > -deltaY)
            {
                error -= deltaY;
                x1 += signX;
            }
            if(error2 < deltaX)
            {
                error += deltaX;
                y1 += signY;
            }
        }
    }

    void draw(SDL_Renderer *renderer) override {
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);

        for (int i = 0; i < mt->n; i++) {
            for (int j = 0; j < mt->m; j++) {
                dm->mas[i][j] = mt->mas[i][j];
            }
        }

        rotate(angle);

        clearBitmap();
        drawLine(mt->mas[0][0], mt->mas[0][1],
                 mt->mas[1][0], mt->mas[1][1]);
        drawLine(mt->mas[1][0], mt->mas[1][1],
                 mt->mas[2][0], mt->mas[2][1]);
        drawLine(mt->mas[2][0], mt->mas[2][1],
                 mt->mas[3][0], mt->mas[3][1]);
        drawLine(mt->mas[3][0], mt->mas[3][1],
                 mt->mas[0][0], mt->mas[0][1]);

        zakr(SPACE + (mt->mas[0][0] + mt->mas[2][0]) / 2, SPACE + (mt->mas[0][1] + mt->mas[2][1]) / 2);

        for (int i = SPACE; i < SPACE + WIDTH; i++) {
            for (int j = SPACE; j < SPACE + HEIGHT; j++) {
                if (bitmap[i][j] == 1) {
                    SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
                    SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
                }
            }
        }

        for (int i = 0; i < mt->n; i++) {
            for (int j = 0; j < mt->m; j++) {
                mt->mas[i][j] = dm->mas[i][j];
            }
        }
    }

    void move(double dx, double dy) override {
        auto *move = new Matrix();
        move->generateMoveMatrix(dx, dy);
        mt = mt->mul(move);
        mt->printMatrix();
    }

    void moveDm(double dx, double dy)  {
        auto *move = new Matrix();
        move->generateMoveMatrix(dx, dy);
        dm = dm->mul(move);
    }

    void scale(double sx, double sy) override {
        auto *scale = new Matrix();
        scale->generateScaleMatrix(sx, sy);

        moveToZero();
        mt = mt->mul(scale);
        moveAfter();

        mt->printMatrix();
    }


    void rotate(double angle) override {
        auto rotate = new Matrix();
        rotate->createRotateMatrix(angle);
        moveToZero();
        mt = mt->mul(rotate);
        normalizeCoordinates();
        moveAfter();
        mt->printMatrix();
    }

    void changeAngle(double angle) override {
        this->angle += angle;
    }

private:

    double ox, oy;

    void moveToZero() {
        ox = - (mt->mas[0][0] + mt->mas[2][0]) / 2;
        oy = - (mt->mas[0][1] + mt->mas[2][1]) / 2;
        move(ox, oy);
        ox = -ox;
        oy = -oy;
    }

    void moveAfter() {
        move(ox, oy);
    }

    void normalizeCoordinates() {
        for (int i = 0; i < mt->n; i++) {
            for (int j = 0; j < mt->m; j++) {
                mt->mas[i][j] /= mt->mas[i][mt->m-1];
            }
        }
    }

    void normalizeCoordinatesDm() {
        for (int i = 0; i < dm->n; i++) {
            for (int j = 0; j < dm->m; j++) {
                dm->mas[i][j] /= dm->mas[i][mt->m - 1];
            }
        }
    }

    void moveToZeroDm() {
        ox = - (dm->mas[0][0] + dm->mas[2][0]) / 2;
        oy = - (dm->mas[0][1] + dm->mas[2][1]) / 2;
        moveDm(ox, oy);
        ox = -ox;
        oy = -oy;
    }

    void moveAfterDm() {
        moveDm(ox, oy);
    }

};


#endif //LABA_SQUARE_H
